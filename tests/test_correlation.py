from nose.tools import with_setup
import numpy as np
from helpers.CorrelationFunction import *
from helpers.CorrelationFunction import _fast_histogram1d


def test_histogram():
    for i in range(3):
        p = np.random.rand(100)
        bins = np.linspace(0, 1, 11)
        k = np.histogram(p, bins)[0]
        assert (k == _fast_histogram1d(p, bins)).all()


points = None

def gen_points():
    global points
    if points is None:
        points = np.mgrid[0:1:21j,0:1:21j,0:1:21j]
        points = points.reshape(3, -1).T
        points[:,0] **= 0.5
        points[:,1] **= 2.0
        points[:,2] **= 0.3
        points[::2] **= 3.0
        points[::3] **= 0.2
        points[::5] **= 4.0
    return points


def get_pairs_brute_force(points1, points2, max_radius, max_dz=None, periodic_box_size=None):
    if periodic_box_size is not None:
        half_box_size = periodic_box_size*0.5
    pairs = []

    for i, p in enumerate(points1):
        d = points2 - p
        if periodic_box_size is not None:
            d[d > half_box_size] -= periodic_box_size
            d[d < -half_box_size] += periodic_box_size
        if max_dz is None:
            d *= d
            d = d.sum(axis=1)
            np.sqrt(d, out=d)
            idx = np.where(d < max_radius)[0]
        else:
            d[:,:2] *= d[:,:2]
            d[:,0] += d[:,1]
            np.sqrt(d[:,0], out=d[:,0])
            np.fabs(d[:,2], out=d[:,2])
            idx = np.where((d[:,0] < max_radius) & (d[:,2] < max_dz))[0]
            d = d[:,0]
        if len(idx):
            d = d[idx]
            pairs.extend((i, j, d1) for j, d1 in zip(idx, d))
    return np.array(pairs, dtype=np.dtype([('id1', np.int64), ('id2', np.int64), ('d', np.float64)]))


def compare_pairs(pairs1, pairs2):
    assert len(pairs1) == len(pairs2)
    pairs1.sort(order=['id1', 'id2'])
    pairs2.sort(order=['id1', 'id2'])
    assert np.allclose(pairs1['d'], pairs2['d'])


@with_setup(gen_points)
def test_get_pairs_sphere():
    args = (points, points, 0.05)
    compare_pairs(get_pairs_brute_force(*args), get_pairs(*args))


@with_setup(gen_points)
def test_get_pairs_sphere_periodic():
    args = (points, points, 0.05, None, 1.0)
    compare_pairs(get_pairs_brute_force(*args), get_pairs(*args))


@with_setup(gen_points)
def test_get_pairs_cylinder():
    args = (points, points, 0.03, 0.1)
    compare_pairs(get_pairs_brute_force(*args), get_pairs(*args))


@with_setup(gen_points)
def test_get_pairs_cylinder_periodic():
    args = (points, points, 0.03, 0.1, 1.0)
    compare_pairs(get_pairs_brute_force(*args), get_pairs(*args))


@with_setup(gen_points)
def test_get_pairs_shpere_auto():
    max_radius = 0.05
    pairs1 = get_pairs(points, points, max_radius)
    pairs1 = pairs1[pairs1['id1'] < pairs1['id2']]
    pairs2 = get_pairs(points, None, max_radius)
    compare_pairs(pairs1, pairs2)


@with_setup(gen_points)
def test_get_pairs__cylinder_auto():
    max_radius = 0.03
    max_z = 0.1
    pairs1 = get_pairs(points, points, max_radius, max_z)
    pairs1 = pairs1[pairs1['id1'] < pairs1['id2']]
    pairs2 = get_pairs(points, None, max_radius, max_z)
    compare_pairs(pairs1, pairs2)


@with_setup(gen_points)
def test_wprp():
    wp1 = projected_correlation(points, np.logspace(-2.5, -1, 11), 0.1, 1.0)
    wp2, _ = projected_correlation(points, np.logspace(-2.5, -1, 11), 0.1, 1.0, jackknife_nside=3)
    assert np.allclose(wp1, wp2)
