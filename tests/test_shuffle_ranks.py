import numpy as np
from scipy.stats import spearmanr
from helpers.shuffleRanks import *

def test_shuffle_ranks():
    x = np.linspace(0, 1, 1001)
    y = shuffle_ranks(x, 0.5)
    assert (y>=0).all()
    assert (y<=1).all()
    assert abs(spearmanr(x, y).correlation-0.5) < 0.05
