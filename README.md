# helpers

This repository contains a collection of useful, but not necessarily related (and certainly not well documented) Python scripts that I use often 
to carry out or accelerate various tasks, most of which involve dark matter simulations.

In particular, you can find Pyhton scripts that

- load the Gadget-2 snapshots,
- load the ascii outputs from Peter Behroozi's Rockstar and Consistent Trees,
- caluclate projected correlation functions with error estimation from jackknife,

and many more other things! If you need help with `helpers`, feel free to contact me at yymao.astro@gmail.com. 


## Installation

    pip install https://bitbucket.org/yymao/helpers/get/master.zip
    