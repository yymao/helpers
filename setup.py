"""
Project website: https://bitbucket.org/yymao/helpers
Copyright (c) 2015-2025 Yao-Yuan Mao (yymao)
"""

from setuptools import setup, find_packages

setup(
    name='helpers',
    version='0.4.3',
    description='A collection of some useful, but not necessarily related, Python scripts that carry out or accelerate various tasks, most of which involve dark matter simulations.',
    url='https://bitbucket.org/yymao/helpers',
    author='Yao-Yuan Mao',
    author_email='yymao.astro@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Astronomy',
    ],
    packages=find_packages(),
    install_requires=['future', 'requests', 'numpy', 'scipy', 'fast3tree', 'astropy', 'pandas'],
    entry_points={
        'console_scripts': [
            'helpers-readGadgetSnapshot=helpers.readGadgetSnapshot:main',
            'helpers-getMUSICregion=helpers.getMUSICregion:main',
            'helpers-getSDSSId=helpers.getSDSSId:main',
            'helpers-distributeMUSICBndryPart=helpers.distributeMUSICBndryPart:main',
        ],
    },
)
