from __future__ import division
from math import pi

__all__ = ['print_sci_notation', 'calc_Delta_vir']

def print_sci_notation(number, decimal_points=3, trim_trailing_zeros=False, with_latex_dollar_sign=True):
    m, _, n = '{{:.{:d}E}}'.format(decimal_points).format(number).partition('E')
    if trim_trailing_zeros:
        m = m.rstrip('0').rstrip('.')
    n = int(n)
    dollar_sign = '$' if with_latex_dollar_sign else ''
    power = '\\times 10^{{{:d}}}'.format(n) if n else ''
    return ''.join((dollar_sign, m, power, dollar_sign))


def calc_Delta_vir(Omega_M0, a=1.0):
    x = 1.0/((1.0/Omega_M0-1.0)*a*a*a + 1.0) - 1.0
    return 18.0*pi*pi + (82.0-39.0*x)*x
