from __future__ import division
import numpy as np
from scipy.stats import rankdata
from scipy.special import erf

__all__ = ['get_ranks', 'shuffle_ranks']


def _f1(x, a):
    y = x/a
    t1 = erf(y)
    t1 *= x
    y *= y
    np.negative(y, out=y)
    np.exp(y, out=y)
    y *= (a/np.sqrt(np.pi))
    y += t1
    return y


def _f2(x, a):
    a = a * np.sqrt(2.0)
    y = _f1(x, a)
    y -= _f1(x-1.0, a)
    y += 1.0
    y /= 2.0
    return y


def get_ranks(data):
    rank = rankdata(data, 'ordinal') - 0.5
    rank /= len(data)
    return rank


def shuffle_ranks(original_ranks, correlation, strict_ranking=False, check_original_ranks=False):
    if check_original_ranks:
        assert (original_ranks >= 0).all() and (original_ranks <= 1.0).all()

    if correlation >= 1.0:
        return original_ranks
    if correlation <= -1.0:
        return 1.0 - original_ranks

    positive_correlation = (correlation > 0)
    correlation_magnitude = abs(correlation)

    if correlation_magnitude < 1e-8:
        ranks = np.random.rand(*original_ranks.shape)
        return get_ranks(ranks) if strict_ranking else ranks

    x = (1.0-correlation_magnitude)/correlation_magnitude
    scale = (x**0.37 * (1.0+x**0.6) * 0.25)
    ranks = np.random.normal(loc=(original_ranks if positive_correlation else (1.0-original_ranks)), scale=scale)

    return get_ranks(ranks) if strict_ranking else _f2(ranks, scale)
