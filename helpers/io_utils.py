from __future__ import print_function

__all__ = ['fits2pandas', 'hlist2pandas', 'get_fits_columns']

import re
from builtins import zip
import numpy as np
import pandas as pd
from astropy.io import fits
from helpers.SimulationAnalysis import BaseParseFields


def get_fits_columns(fname):
    """
    Retrive the columns of a FITS table.

    Parameters
    ----------
    fname : file path
        FITS file to be opened.

    Returns
    -------
    columns : tuple
    """
    f = fits.open(fname)
    try:
        columns = tuple(f[1].data.names)
    finally:
        f.close()
    return columns


def fits2pandas(fname, include_only=None, exclude=None):
    """
    Read and convert FITS table to pandas DataFrame.
    Support multi-dimension columns.

    Parameters
    ----------
    fname : file path
        FITS file to be opened.
    include_only : sequence of str, default None
        Columns or fields to include only; ignore all other columns.
    exclude : sequence of str, default None
        Columns or fields to exclude. Ignored if `include_only` is specified.

    Returns
    -------
    df : DataFrame
    """
    f = fits.open(fname)
    try:
        data = f[1].data

        if include_only:
            if not all(n in data.names for n in include_only):
                raise ValueError('Some names in `include_only` are not available. Available columns are:\n{}'.format(', '.join(data.names)))
            if exclude:
                print('Warning: `exclude` is ignored because `include_only` is specified.')
            names = include_only
        elif exclude:
            exclude = set(exclude)
            names = [n for n in data.names if n not in exclude]
        else:
            names = data.names

        df = []
        for name in names:
            if data[name].ndim > 1:
                for s in np.ndindex(*data[name].shape[1:]):
                    new_name = name + ''.join('[{}]'.format(i) for i in s)
                    df.append(pd.Series(data[name][(slice(None),) + s].byteswap().newbyteorder(), name=new_name))
            else:
                df.append(pd.Series(data[name].byteswap().newbyteorder(), name=name))
    finally:
        f.close()
    return pd.concat(df, axis=1, join="inner")


def hlist2pandas(hlist, fields=None, sanitize_column_names=False, **kwargs):
    """
    Read hlist as a pandas DataFrame.

    Parameters
    ----------
    hlist : str
        The path to the file
    fields : str, int, array_like, optional
        The desired fields. It can be a list of string or int. If fields is None (default), return all the fields listed in the header
    sanitize_column_names : bool
    kwargs : dict
        Other keyword arguments are passed to pd.read_csv

    Returns
    -------
    df : DataFrame
    """
    header = pd.read_csv(hlist, nrows=1, dtype=str, \
            engine='c', delim_whitespace=True, header=None, \
            skipinitialspace=True, na_filter=False, quotechar=' ').iloc[0].tolist()
    if not header:
        raise ValueError('Cannot obtain header')
    header[0] = header[0].lstrip('#')
    try:
        hlist.seek(0, 0)
    except (TypeError, AttributeError):
        pass
    header = [re.sub(r'\(\d+\)$', '', s) for s in header]
    if sanitize_column_names:
        header = [re.sub(r'[_\W]+', '_', s).strip('_').lower() for s in header]
    p = BaseParseFields(header, fields)
    k = np.argsort(p._usecols)
    return pd.read_csv(hlist, comment='#',\
            names=[p._names[i] for i in k], \
            usecols=[p._usecols[i] for i in k], \
            dtype=dict(zip(p._names, p._formats)), \
            engine='c', delim_whitespace=True, header=None, \
            skipinitialspace=True, na_filter=False, quotechar=' ', **kwargs)
